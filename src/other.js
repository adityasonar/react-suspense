import React from 'react';

export default function OtherComponent(){
    return(
        <div>
            <iframe src='https://upload.wikimedia.org/wikipedia/commons/6/64/Heavy.jpg' width={400}/>
            This is the Other component which is 'Lazy' loaded.
        </div>
    )
}