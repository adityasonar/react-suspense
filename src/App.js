import React from 'react';
import { Suspense } from 'react';
const OtherComponent = React.lazy(() => import('./other'));

class App extends React.Component{
  constructor(){
    super();
    this.state={
      proceed: false,
    }
  }
  
  componentDidMount(){
    // this.allowProceed();
    setTimeout(()=>this.allowProceed(),5000);
  }

  allowProceed=()=>{
    this.setState({
      proceed: true
    });
  }

  render(){
  return (
    <div className="App">
    <Suspense fallback={<div>Climax loading in suspense</div>}>
    {this.state.proceed &&
      <header className="App-header">
        <OtherComponent />
      </header>
    }
    </Suspense>
    </div>
  );
}
}

export default App;
